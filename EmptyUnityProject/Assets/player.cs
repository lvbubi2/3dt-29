﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
    public GameObject Camera;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 movement = new Vector3(
            Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")
            );
        GetComponent<Rigidbody>().AddForce(movement * 20);

        Vector3 position = transform.position;
        Camera.transform.position = new Vector3(
            position.x,
            10,
            position.z
            );
	}
}
